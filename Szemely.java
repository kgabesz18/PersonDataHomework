package kg.person;

public class Szemely {
	
	private String vezetekNev;
	private String keresztNev;
	private String kozepsoNev;
	private int eletKor;
	
	public Szemely(String vezetekNev, String keresztNev, String kozepsoNev, int eletkor) {
		this.vezetekNev = vezetekNev;
		this.keresztNev = keresztNev;
		this.kozepsoNev = kozepsoNev;
		this.eletKor = eletkor;
	}
	
	public String getVezetekNev() {
		return this.vezetekNev;
	}
	
	public String getKeresztNev() {
		return this.keresztNev;
	}
	
	public String getKozepsoNev() {
		return this.kozepsoNev;
	}
	
	public int getEletKor() {
		return this.eletKor;
	}
	
	public void setVezetekNev(String vezetekNev) {
		this.vezetekNev = vezetekNev;
	}
	
	public void setKeresztNev(String keresztNev) {
		this.keresztNev = keresztNev;
	}
	
	public void setKozepsoNev(String kozepsoNev) {
		this.kozepsoNev = kozepsoNev;
	}
	
	public void setEletKor(int eletKor) {
		this.eletKor = eletKor;
	}

}
