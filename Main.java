package kg.person;

public class Main {

	public static void main(String[] args) {
		
		Szemely elsoSzemely = new Szemely("S�ndor", "J�zsef", "Benedek", 35);
		Szemely masodikSzemely = new Szemely("Megyeri", "Andr�s", "Zolt�n", 44);
		Szemely harmadikSzemely = new Szemely("Kov�cs", "Istv�n", "K�roly", 25);
		
		System.out.println("Elso szemely adatai:");
		System.out.printf("Vezeteknev: %s\n Kozepsonev: %s\n Keresztnev: %s\n Eletkor: %d\n",
				elsoSzemely.getVezetekNev(), elsoSzemely.getKozepsoNev(), elsoSzemely.getKeresztNev(), elsoSzemely.getEletKor());
		System.out.println();
		System.out.println("Masodik szemely adatai:");
		System.out.printf("Vezeteknev: %s\n Kozepsonev: %s\n Keresztnev: %s\n Eletkor: %d\n",
				masodikSzemely.getVezetekNev(), masodikSzemely.getKozepsoNev(), masodikSzemely.getKeresztNev(), masodikSzemely.getEletKor());
		System.out.println();
		System.out.println("Harmadik szemely adatai:");
		System.out.printf("Vezeteknev: %s\n Kozepsonev: %s\n Keresztnev: %s\n Eletkor: %d\n",
				harmadikSzemely.getVezetekNev(), harmadikSzemely.getKozepsoNev(), harmadikSzemely.getKeresztNev(), harmadikSzemely.getEletKor());

	}

}
